export const geojson = {
  createFeatureCollection(features) {
    const fc = {
      type: 'FeatureCollection',
      features: []
    };

    features.forEach(feature => {
      fc.features.push(JSON.parse(feature.geojson));
    });

    return fc;
  }
};
