import dotenv from 'dotenv';
import express from 'express';
import { resolve } from 'path';

import routes from './routes';

dotenv.config();

express()
  .get('/heatmap', (_, res) => {
    res.redirect('/');
  })
  .use('/api', routes)
  .use(express.static(resolve(process.env.PUBLIC_ROOT)))
  .set('env', process.env.NODE_ENV)
  .set('host', process.env.HOST || '0.0.0.0')
  .set('port', process.env.PORT || 80)
  .listen(process.env.PORT, process.env.HOST, err => {
    err
      ? console.error('Server Failed:\n', err)
      : console.log(
          `Active on http://localhost:${process.env.PORT} at ` +
            `${new Date().toDateString()} ${new Date().toTimeString()}`
        );
  });
