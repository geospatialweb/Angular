import { LayerElement } from '@map/interfaces/layerElement/layer-element.interface';
import { Layerstyle } from '@map/interfaces/layerstyle/layerstyle.interface';
import { Trail } from '@map/interfaces/trail/trail.interface';

export interface Store {
  accessToken: string;
  heatmap: any;
  layerElements: LayerElement[];
  layerstyles: Layerstyle[];
  map: any;
  markers: any;
  popup: any;
  splashScreen: any;
  trails: Trail[];
}
