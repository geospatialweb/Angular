export interface HeatmapSettings {
  bearing: number;
  center: number[];
  pitch: number;
  zoom: number;
}
