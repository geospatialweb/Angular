import { Component } from '@angular/core';

@Component({
  selector: 'app-heatmap',
  styleUrls: ['heatmap.component.scss'],
  templateUrl: 'heatmap.component.html'
})
export class HeatmapComponent {}
