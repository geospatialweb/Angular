import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { HeatmapUIComponent } from './heatmap-ui.component';

describe('HeatmapUIComponent', () => {
  let component: HeatmapUIComponent;
  let fixture: ComponentFixture<HeatmapUIComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HeatmapUIComponent],
      imports: [RouterTestingModule],
      providers: []
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeatmapUIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => expect(component).toBeTruthy());
});
