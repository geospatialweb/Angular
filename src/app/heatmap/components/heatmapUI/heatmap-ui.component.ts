import { EventEmitter, Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';

import { EventEmitterService } from '@app/services/eventEmitter/event-emitter.service';
import { StoreService } from '@store/services/store.service';

@Component({
  selector: 'app-heatmap-ui',
  styleUrls: ['heatmap-ui.component.scss'],
  templateUrl: 'heatmap-ui.component.html',
  animations: [
    trigger('fade', [
      state(
        'fade-out',
        style({
          opacity: 0,
          transform: 'translateX(0) scale(0)'
        })
      ),
      state(
        'fade-in',
        style({
          opacity: 1,
          transform: 'translateX(0) scale(1)'
        })
      ),
      transition('fade-out => fade-in', animate(1500))
    ])
  ]
})
export class HeatmapUIComponent implements OnInit {
  private eventEmitter: EventEmitter<any>;

  fade = 'fade-out';

  constructor(private eventEmitterService: EventEmitterService, public storeService: StoreService) {
    this.eventEmitter = this.eventEmitterService.eventEmitter;
  }

  ngOnInit(): void {
    setTimeout(() => (this.fade = 'fade-in'));
  }

  resetCoords(): void {
    this.eventEmitter.emit(['resetHeatmapSettings']);
  }

  resetParams(): void {
    this.eventEmitter.emit(['resetHeatmapParams']);
  }

  return(): void {
    this.eventEmitter.emit(['setRoutePath', '/']);
  }
}
