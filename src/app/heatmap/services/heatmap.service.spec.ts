import { TestBed } from '@angular/core/testing';

import { HeatmapService } from './heatmap.service';

describe('HeatmapService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: []
    });
  });

  it('should be created', () => {
    const service: HeatmapService = TestBed.inject(HeatmapService);
    expect(service).toBeTruthy();
  });
});
