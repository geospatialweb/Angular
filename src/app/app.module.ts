import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from '@app/components/app.component';
import { HeaderComponent } from '@app/components/header/header.component';
import { SplashComponent } from '@app/components/splash/splash.component';

@NgModule({
  declarations: [AppComponent, HeaderComponent, SplashComponent],
  imports: [AppRoutingModule, BrowserModule, BrowserAnimationsModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
