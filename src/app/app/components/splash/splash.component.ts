import { Component } from '@angular/core';

import { StoreService } from '@store/services/store.service';

@Component({
  selector: 'app-splash',
  styleUrls: ['splash.component.scss'],
  templateUrl: 'splash.component.html'
})
export class SplashComponent {
  constructor(public storeService: StoreService) {}
}
