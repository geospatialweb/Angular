import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as mapboxgl from 'mapbox-gl';

import { EventEmitterService } from '@app/services/eventEmitter/event-emitter.service';

@Injectable({
  providedIn: 'root'
})
export class AccessTokenService {
  private eventEmitter: EventEmitter<any>;

  constructor(private httpClient: HttpClient, private eventEmitterService: EventEmitterService) {
    this.eventEmitter = this.eventEmitterService.eventEmitter;
  }

  async getAccessToken(): Promise<void> {
    await this.httpClient
      .get('/api/accessToken')
      .toPromise()
      .then((accessToken: string) => {
        this.setAccessToken(accessToken);
      })
      .catch((err: Error) => {
        console.error('setAccessToken Failed:\n', err);
      });
  }

  private setAccessToken(accessToken: string): void {
    (mapboxgl as any).accessToken = accessToken;
    this.eventEmitter.emit(['setAccessToken', accessToken]);
  }
}
