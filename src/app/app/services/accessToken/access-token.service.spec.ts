import { TestBed } from '@angular/core/testing';
import { HttpClient, HttpHandler } from '@angular/common/http';

import { AccessTokenService } from './access-token.service';

describe('AccessTokenService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [HttpClient, HttpHandler]
    });
  });

  it('should be created', () => {
    const service: AccessTokenService = TestBed.inject(AccessTokenService);
    expect(service).toBeTruthy();
  });
});
