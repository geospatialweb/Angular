import { EventEmitter, Injectable } from '@angular/core';

import { AccessTokenService } from '@app/services/accessToken/access-token.service';
import { EventEmitterService } from '@app/services/eventEmitter/event-emitter.service';
import { StoreService } from '@store/services/store.service';
import { SubscriptionService } from '@app/services/subscription/subscription.service';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  private eventEmitter: EventEmitter<any>;

  constructor(
    private accessTokenService: AccessTokenService,
    private eventEmitterService: EventEmitterService,
    private storeService: StoreService,
    private subscriptionService: SubscriptionService
  ) {
    this.eventEmitter = this.eventEmitterService.eventEmitter;
  }

  createStore(): void {
    this.storeService.createStore();
    this.setSubscriptions();
  }

  private setSubscriptions(): void {
    this.subscriptionService.setSubscriptions();
    this.getData();
  }

  private getData(): void {
    this.eventEmitter.emit(['getData']);
    this.loadMap();
  }

  private async loadMap(): Promise<void> {
    await this.accessTokenService.getAccessToken();
    /* refer to setRoutes subscription */
    this.eventEmitter.emit(['setRoutePath', '/']);
  }
}
