import { TestBed } from '@angular/core/testing';

import { MarkerService } from './marker.service';

describe('MarkerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: []
    });
  });

  it('should be created', () => {
    const service: MarkerService = TestBed.inject(MarkerService);
    expect(service).toBeTruthy();
  });
});
