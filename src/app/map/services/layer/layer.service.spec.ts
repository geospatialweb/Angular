import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { LayerService } from './layer.service';

describe('LayerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: []
    });
  });

  it('should be created', () => {
    const service: LayerService = TestBed.inject(LayerService);
    expect(service).toBeTruthy();
  });
});
