import { TestBed } from '@angular/core/testing';

import { LayerstyleService } from './layerstyle.service';

describe('LayerstyleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: []
    });
  });

  it('should be created', () => {
    const service: LayerstyleService = TestBed.inject(LayerstyleService);
    expect(service).toBeTruthy();
  });
});
