import { Injectable } from '@angular/core';

import { Layerstyle } from '@map/interfaces/layerstyle/layerstyle.interface';

@Injectable({
  providedIn: 'root'
})
export class LayerstyleService {
  private layerstylesHash: { biosphere: number; trails: number };

  layerstyles: Layerstyle[];

  constructor() {
    this.layerstyles = [];
    this.layerstylesHash = {
      biosphere: null,
      trails: null
    };
  }

  setLayerstyleActive(id: string): void {
    const i = this.layerstylesHash[id];

    this.layerstyles[i].active = !this.layerstyles[i].active;
    this.layerstyles[i].active
      ? (this.layerstyles[i].layout.visibility = 'visible')
      : (this.layerstyles[i].layout.visibility = 'none');
  }

  setLayerstyles(layerstyle: Layerstyle): void {
    this.layerstyles.push(layerstyle);
    this.layerstylesHash[layerstyle.id] = this.layerstyles.length - 1;
  }
}
