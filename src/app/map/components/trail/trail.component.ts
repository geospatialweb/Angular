import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';

import { StoreService } from '@store/services/store.service';

@Component({
  selector: 'app-trails',
  styleUrls: ['trail.component.scss'],
  templateUrl: 'trail.component.html',
  animations: [
    trigger('slide', [
      state(
        'slide-out',
        style({
          transform: 'translateX(-150px)'
        })
      ),
      state(
        'slide-in',
        style({
          transform: 'translateX(0)'
        })
      ),
      transition('slide-out => slide-in', animate(1000))
    ])
  ]
})
export class TrailComponent implements OnInit {
  slide = 'slide-out';

  constructor(public storeService: StoreService) {}

  ngOnInit(): void {
    setTimeout(() => (this.slide = 'slide-in'));
  }
}
