import { Component, OnInit } from '@angular/core';
import { state, style, trigger } from '@angular/animations';

import { StoreService } from '@store/services/store.service';

@Component({
  selector: 'app-icons',
  styleUrls: ['icon.component.scss'],
  templateUrl: 'icon.component.html',
  animations: [
    trigger('visible', [
      state(
        'hidden',
        style({
          opacity: 0
        })
      ),
      state(
        'visible',
        style({
          opacity: 1
        })
      )
    ])
  ]
})
export class IconComponent implements OnInit {
  visible = 'hidden';

  constructor(public storeService: StoreService) {}

  ngOnInit(): void {
    setTimeout(() => (this.visible = 'visible'), 1100);
  }
}
