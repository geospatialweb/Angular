import { Directive, ElementRef, OnInit } from '@angular/core';
import { fromEvent } from 'rxjs';

import { EventEmitterService } from '@app/services/eventEmitter/event-emitter.service';

@Directive({
  selector: '[appLayerClick], [appIconClick]'
})
export class LayerElementDirective implements OnInit {
  constructor(private el: ElementRef, private eventEmitterService: EventEmitterService) {}

  ngOnInit(): void {
    /* RxJS click handler subscription for the selectors above */
    const onClick = fromEvent(this.el.nativeElement, 'click').subscribe({
      next: (evt: MouseEvent) => {
        evt.stopPropagation();
        const id: string = (evt as any).target.id.split('-')[0];
        this.eventEmitterService.eventEmitter.emit(['selectLayer', id]);
      },
      error: (err: Error) => {
        console.error('onClick Failed:\n', err);
      },
      complete: () => {
        onClick.unsubscribe();
      }
    });
  }
}
