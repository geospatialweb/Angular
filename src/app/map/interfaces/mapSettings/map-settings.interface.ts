import { LngLatBoundsLike } from 'mapbox-gl';

export interface MapSettings {
  bearing: number;
  bounds: LngLatBoundsLike;
  center: number[];
  pitch: number;
  style: string;
  zoom: number;
}
