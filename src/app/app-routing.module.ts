import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { HeatmapComponent } from '@heatmap/components/heatmap.component';
import { HeatmapUIComponent } from '@heatmap/components/heatmapUI/heatmap-ui.component';
import { IconComponent } from '@map/components/icon/icon.component';
import { LayerComponent } from '@map/components/layer/layer.component';
import { MapComponent } from '@map/components/map.component';
import { MapUIComponent } from '@map/components/mapUI/map-ui.component';
import { TrailComponent } from '@map/components/trail/trail.component';

import { LayerElementDirective } from '@map/directives/layerElement/layer-element.directive';
import { TrailDirective } from '@map/directives/trail/trail.directive';

const routes: Routes = [
  { path: '', component: MapComponent },
  { path: 'heatmap', component: HeatmapComponent }
];

@NgModule({
  declarations: [
    HeatmapComponent,
    HeatmapUIComponent,
    IconComponent,
    LayerComponent,
    MapComponent,
    MapUIComponent,
    TrailComponent,
    LayerElementDirective,
    TrailDirective
  ],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, {
      onSameUrlNavigation: 'reload'
    })
  ],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {}
