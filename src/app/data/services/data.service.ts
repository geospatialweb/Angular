import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import * as fetch from 'd3-fetch';
import { FeatureCollection } from 'geojson';
import { Subscription } from 'rxjs';

import { Layerstyle } from '@map/interfaces/layerstyle/layerstyle.interface';

import { EventEmitterService } from '@app/services/eventEmitter/event-emitter.service';

import { layerstyles } from '@data/data/layerstyle/layerstyle.data';
import { markers } from '@data/data/marker/marker.data';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private eventEmitter: EventEmitter<any>;
  private layerstyles: any[];
  private markers: any[];

  constructor(private httpClient: HttpClient, private eventEmitterService: EventEmitterService) {
    this.eventEmitter = this.eventEmitterService.eventEmitter;
    this.layerstyles = layerstyles;
    this.markers = markers;
  }

  getData(): void {
    this.getHeatmapData();
    this.getLayerstyles();
    this.getMarkers();
  }

  private getHeatmapData(): void {
    fetch
      .csv('https://raw.githubusercontent.com/uber-common/deck.gl-data/master/examples/3d-heatmap/heatmap-data.csv')
      .then((data: any[]) => {
        if (data.length) {
          return this.eventEmitter.emit(['setHeatmapData', data]);
        }

        console.error('getHeatmapData Data Error:\n', data);
      })
      .catch((err: Error) => {
        console.error('getHeatmapData Failed:\n', err);
      });
  }

  private getLayerstyles(): void {
    this.layerstyles.forEach((layerstyle: { fields: string; layer: { id: string } }, i: number) => {
      let params: HttpParams = new HttpParams();
      params = params.set('fields', layerstyle.fields);
      params = params.set('table', layerstyle.layer.id);

      const getLayerstyle: Subscription = this.httpClient.get('/api/geojson', { params }).subscribe({
        next: (fc: FeatureCollection) => {
          if (fc.features.length) {
            const layerstyle: Layerstyle = this.layerstyles[i].layer;
            layerstyle.source.data = fc;
            return this.eventEmitter.emit(['setLayerstyles', layerstyle]);
          }

          console.log('getLayerstyle No Features Found:\n', fc);
        },
        error: (err: HttpErrorResponse) => {
          console.error('getLayerstyle Failed:\n', err);
        },
        complete: () => {
          getLayerstyle.unsubscribe();
        }
      });
    });
  }

  private getMarkers(): void {
    this.markers.forEach((marker: { fields: string; id: string }) => {
      let params: HttpParams = new HttpParams();
      params = params.set('fields', marker.fields);
      params = params.set('table', marker.id);

      const getMarker: Subscription = this.httpClient.get('/api/geojson', { params }).subscribe({
        next: (fc: FeatureCollection) => {
          if (fc.features.length) {
            return this.eventEmitter.emit(['setMarkers', fc, marker.id]);
          }

          console.log('getMarker No Features Found:\n', fc);
        },
        error: (err: HttpErrorResponse) => {
          console.error('getMarker Failed:\n', err);
        },
        complete: () => {
          getMarker.unsubscribe();
        }
      });
    });
  }
}
