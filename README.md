# Geospatial Web

<http://www.geospatialweb.ca>

Development sandbox website to showcase the code integration of Node, Express, Angular 9, RxJS, Mapbox GL, Deck.GL, PostGIS 3.0 and Docker. This implementation features dedicated Map instances in separate routes and `Angular Model` simple state management with immutable data exposed as RxJS Observable: <https://tomastrajan.github.io/angular-model-pattern-example#/about>.
