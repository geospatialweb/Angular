import express from 'express';
import { Pool } from 'pg';

import { geojson } from '../modules';

export default express
  .Router()
  .get('/accessToken', (_, res) => res.status(200).json(JSON.parse(JSON.stringify(process.env.ACCESSTOKEN))))
  .get('/geojson', (req, res) => {
    const query = `
      SELECT ST_AsGeoJSON(feature.*) AS geojson
      FROM (
        SELECT ${req.query.fields}
        FROM ${req.query.table}
      ) AS feature
    `;

    const pool = new Pool({
      /* docker instance: process.env.DATABASE_URI */
      /* local instance:  process.env.DATABASE_URI_LOCAL */
      connectionString: process.env.DATABASE_URL_LOCAL
    });

    pool
      .query(query)
      .then(result => {
        const fc = geojson.createFeatureCollection(result.rows);
        res.status(200).json(fc);
        return pool.end();
      })
      .catch(err => {
        console.error('Query Failed:\n', err);
        return pool.end();
      });
  });
